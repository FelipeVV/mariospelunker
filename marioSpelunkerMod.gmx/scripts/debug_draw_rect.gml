///debug_draw_rect(x1, y1, x2, y2)

x1_pass = argument0;
y1_pass = argument1;
x2_pass = argument2;
y2_pass = argument3;

with( instance_create(x, y, obj_debugrect) )
{
    x1 = other.x1_pass;
    y1 = other.y1_pass;
    x2 = other.x2_pass;
    y2 = other.y2_pass;
}
