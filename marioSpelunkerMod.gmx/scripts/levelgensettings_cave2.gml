/// VERY IMPORTANT Generation properties

show_debug_message("cave 2 create event ");

// Name at the start of every text file to check for generation
file_location = "LevelGen_Files\\cave\\"
level_type = "area";
// How many different types of paths are there for every path (for example, 8 types of "T" and "+")
max_variations = 8;
secret_exit_door = false;
secret_boo_exit_door = false;
// Is this a shortcut level that will need to unlock a door upon ending?
secret_area = false;

/// CHANCES
// all of this numbers mean the probability of selecting 1 out of X

//chance to mirror the selected room.
chance_to_flip_subroom = 2; // 1 out of...
chance_to_eliminate_muncher = 1.7;
chance_to_place_probability_tile = 2;
chance_to_place_brickprobability_tile = 2;

// Flavours! Special stuff that can happen in levels
chance_for_flavor_angrysun = 0;
chance_for_flavor_lakitu = 0;
chance_for_flavor_bobombrain = 20;
chance_for_flavor_storm = 10;
can_spawn_flying_bobombs = false; // always false


// brick to question block
chance_to_substitute_brick_with_bombbrick = 160;
// array of possible outcomes of random_sprout()
block_array[0] = 1;
block_array[1] = 1;
block_array[2] = 1;
block_array[3] = 2;

// enemies in floored tiles
chance_to_spawn_floored_enemy = 33;
floor_enemies_array[0 ] = obj_goomba;
floor_enemies_array[1 ] = obj_thwomp;
floor_enemies_array[2 ] = obj_thwomp;
floor_enemies_array[3 ] = obj_paragoomba;
floor_enemies_array[4 ] = obj_koopa;
floor_enemies_array[5 ] = obj_koopa_red;
floor_enemies_array[6 ] = obj_jumpingkoopa;
floor_enemies_array[7 ] = obj_jumpingkoopa;
floor_enemies_array[8 ] = obj_spiny;
floor_enemies_array[9 ] = obj_spiny;
floor_enemies_array[10] = obj_shoegoomba;
floor_enemies_array[11] = obj_spike;
floor_enemies_array[12] = obj_nokobon;
floor_enemies_array[13] = obj_nokobon;



// enemies in ceiling tiles
chance_to_spawn_ceiling_enemy = 50;
ceiling_enemies_array[0] = obj_fallingspike;
ceiling_enemies_array[1] = obj_fallingspike;
ceiling_enemies_array[2] = obj_fallingspike;
ceiling_enemies_array[3] = obj_swooper;
ceiling_enemies_array[4] = obj_swooper;
ceiling_enemies_array[5] = obj_swooper;
ceiling_enemies_array[6] = obj_swooper;

// enemies in fence tiles
chance_to_spawn_fence_enemy = 42;
fence_enemies_array[0] = obj_netkoopa;
fence_enemies_array[1] = obj_netkoopa;
fence_enemies_array[2] = obj_netkoopa;
fence_enemies_array[3] = obj_netkoopa_red;
fence_enemies_array[4] = obj_netkoopa_red;
fence_enemies_array[5] = obj_netkoopa_hyper;
fence_enemies_array[5] = obj_bubblegoomba;

// enemies in vine tiles
chance_to_spawn_vine_enemy = 90;
vine_enemies_array[0] = obj_hoopster;

/// DICTIONARY
// This transforms the numbers in the .txt into real game objects

ascii_dictionary[0] = noone; // 0 is normally air
ascii_dictionary[1] = obj_unspinbrick;
ascii_dictionary[2] = obj_brick;
ascii_dictionary[3] = obj_vine;
ascii_dictionary[4] = obj_noteblock;
ascii_dictionary[5] = obj_flipblock;
ascii_dictionary[6] = obj_platform_falling;
ascii_dictionary[7] = obj_donut;
ascii_dictionary[8] = obj_net;
ascii_dictionary[9] = obj_fencetop;
ascii_dictionary[10] = obj_muncher;
ascii_dictionary[11] = obj_vinewithplatform;
ascii_dictionary[12] = obj_probability_tile;
ascii_dictionary[13] = obj_brickprobability_tile;


/// Finally call event for rest of generation
// A child object should be able to only change the create event
event_user(0);
