/// VERY IMPORTANT Generation properties

show_debug_message("ice 1 create event ");

// Name at the start of every text file to check for generation
file_location = "LevelGen_Files\\ice\\"
level_type = "ice";
// How many different types of paths are there for every path (for example, 8 types of "T" and "+")
max_variations = 6;
secret_exit_door = false;
secret_boo_exit_door = true;
// Is this a shortcut level that will need to unlock a door upon ending?
secret_area = false;


/// CHANCES
// all of this numbers mean the probability of selecting 1 out of X
//chance to mirror the selected room.
chance_to_flip_subroom = 2; // 1 out of...
chance_to_eliminate_muncher = 1.6;
chance_to_place_probability_tile = 2;

// Flavours! Special stuff that can happen in levels
chance_for_flavor_angrysun = 0;
chance_for_flavor_lakitu = 0;
chance_for_flavor_bobombrain = 12;
chance_for_flavor_storm = 0;
can_spawn_flying_bobombs = false; // always false


chance_to_substitute_brick_with_question_block = 15;
chance_to_substitute_surface_block_with_spring = 35;
chance_to_place_chomper_on_surface_block = 22;

chance_to_substitute_brick_with_bombbrick = 15;
chance_to_spawn_coin = 30; // 5 is too easy to get a 1up
chance_to_spawn_coin_in_digsand = 2.4;
chance_to_spawn_coin_in_ice = 15;
chance_to_spawn_chomper_in_ice = 22;

// array of possible outcomes of random_sprout()
block_array[0] = 13;

// enemies in floored tiles
chance_to_spawn_floored_enemy = 45;
floor_enemies_array[0 ] = obj_bobomb;
floor_enemies_array[1 ] = obj_galoomba;
floor_enemies_array[2 ] = obj_mechakoopa_bomb;
floor_enemies_array[3 ] = obj_buzzybeetle;
floor_enemies_array[5 ] = obj_bobomb;
floor_enemies_array[7 ] = obj_koopa_red;
floor_enemies_array[8 ] = obj_nokobon;

// enemies in ceiling tiles
chance_to_spawn_ceiling_enemy = 10;
ceiling_enemies_array[0] = obj_swooper;
ceiling_enemies_array[1] = obj_swooper;
ceiling_enemies_array[2] = obj_fallingspike_ice;
ceiling_enemies_array[3] = obj_fallingspike_ice;
ceiling_enemies_array[4] = obj_fallingspike_ice;
ceiling_enemies_array[5] = obj_fallingspike_ice;
ceiling_enemies_array[6] = obj_fallingspike_ice;
ceiling_enemies_array[7] = obj_fallingspike_ice;

// enemies in fence tiles
chance_to_spawn_fence_enemy = 150;
fence_enemies_array[0] = obj_netkoopa_hyper;

// enemies in vine tiles
chance_to_spawn_vine_enemy = 1000000;
vine_enemies_array[0] = noone;

// enemies in air
chance_to_spawn_airborne_enemy = 700;
airborne_enemies_array[0] = obj_bubblegoomba;
airborne_enemies_array[1] = obj_galoomba_parachute;
airborne_enemies_array[2] = obj_galoomba_parachute;
airborne_enemies_array[3] = obj_firechomp;

/// DICTIONARY
// This transforms the numbers in the .txt into real game objects
ascii_dictionary[0 ] = noone; // 0 is normally air
ascii_dictionary[1 ] = obj_unspinbrick_ice;
ascii_dictionary[2 ] = obj_brick;
ascii_dictionary[3 ] = obj_probability_tile;
ascii_dictionary[4 ] = obj_unspinbrick_sl;
ascii_dictionary[5 ] = obj_unspinbrick_sr;
ascii_dictionary[6 ] = obj_slippery_ice;
ascii_dictionary[7 ] = obj_fencetop;
ascii_dictionary[8 ] = obj_net;
ascii_dictionary[9 ] = obj_platformspin_here;
ascii_dictionary[10] = obj_noteblock;
ascii_dictionary[11] = obj_flipblock;
ascii_dictionary[12] = obj_muncher_blue;
ascii_dictionary[13] = obj_donut_blue;

/// Finally call event for rest of generation
// A child object should be able to only change the create event
event_user(0);
