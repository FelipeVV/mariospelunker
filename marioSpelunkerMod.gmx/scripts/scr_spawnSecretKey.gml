// choose a different column than the spawning Mario
var col_secret_door = irandom(3);
var row_secret_door = irandom(3);
var secret_door_room_col = 4;
var secret_door_room_row = 16;
with(instance_create(x + (col_secret_door*352) + (secret_door_room_col*16),
                y + (row_secret_door*288) + (secret_door_room_row*16),
                obj_key))
{
    destroy_objects_in_area(x, y, x+16, y+16, obj_blockparent, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_fencetop, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_unspinbrick, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_net, false);
}
