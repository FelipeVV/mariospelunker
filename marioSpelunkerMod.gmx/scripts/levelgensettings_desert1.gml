/// VERY IMPORTANT Generation properties

show_debug_message("desert 1 create event ");

// Name at the start of every text file to check for generation
file_location = "LevelGen_Files\\desert\\"
level_type = "desert";
// How many different types of paths are there for every path (for example, 8 types of "T" and "+")
max_variations = 8;
secret_exit_door = false;
secret_boo_exit_door = false;
// Is this a shortcut level that will need to unlock a door upon ending?
secret_area = false;


/// CHANCES
// all of this numbers mean the probability of selecting 1 out of X
//chance to mirror the selected room.
chance_to_flip_subroom = 2; // 1 out of...
chance_to_eliminate_muncher = 1.6;
chance_to_place_probability_tile = 4;

// Flavours! Special stuff that can happen in levels
chance_for_flavor_angrysun = 20;
chance_for_flavor_lakitu = 25;
chance_for_flavor_bobombrain = 200;
chance_for_flavor_storm = 0;
can_spawn_flying_bobombs = false; // always false
chance_to_spawn_plant_pipe = 15;

chance_to_substitute_brick_with_question_block = 35;
chance_to_substitute_surface_block_with_spring = 200;
chance_to_substitute_brick_with_bombbrick = 75;
chance_to_spawn_coin = 56; // 5 is too easy to get a 1up
chance_to_spawn_coin_in_digsand = 3.5;
// array of possible outcomes of random_sprout()
block_array[0] = 0;
block_array[1] = 1;
block_array[2] = 1;
block_array[3] = 1;
block_array[4] = 2;
block_array[5] = 2;
block_array[6] = 3;

// enemies in floored tiles
chance_to_spawn_floored_enemy = 40;
floor_enemies_array[0 ] = obj_nipper;
floor_enemies_array[1 ] = obj_montymole;
floor_enemies_array[2 ] = obj_montymole;
floor_enemies_array[3 ] = obj_vulcanolotus;
floor_enemies_array[4 ] = obj_dinotorch;
floor_enemies_array[5 ] = obj_brickgoomba;
floor_enemies_array[6 ] = obj_brickgoomba;
floor_enemies_array[7 ] = obj_firesnake;
floor_enemies_array[8 ] = obj_pokey;
floor_enemies_array[9 ] = obj_pokey_green;
floor_enemies_array[10] = obj_nokobon;
floor_enemies_array[11] = obj_nokobon;
floor_enemies_array[12] = obj_mechakoopa_bomb;
floor_enemies_array[13] = obj_firesnake;
floor_enemies_array[14] = obj_bonybeetle;

// enemies in ceiling tiles
chance_to_spawn_ceiling_enemy = 35;
ceiling_enemies_array[0] = obj_swooper;
ceiling_enemies_array[1] = obj_swooper;
ceiling_enemies_array[2] = obj_swooper;
ceiling_enemies_array[3] = obj_swooper;
ceiling_enemies_array[4] = obj_fallingspike;
ceiling_enemies_array[5] = obj_fallingspike;
ceiling_enemies_array[6] = obj_fallingspike;
ceiling_enemies_array[7] = obj_fallingspike;
ceiling_enemies_array[8] = obj_fallingspike;
ceiling_enemies_array[9] = obj_spiny_ceiling;

// enemies in fence tiles
chance_to_spawn_fence_enemy = 1000000;
fence_enemies_array[0] = noone;

// enemies in vine tiles
chance_to_spawn_vine_enemy = 1000000;
vine_enemies_array[0] = noone;

/// DICTIONARY
// This transforms the numbers in the .txt into real game objects

ascii_dictionary[0 ] = noone; // 0 is normally air
ascii_dictionary[1 ] = obj_unspinbrick_desert;
ascii_dictionary[2 ] = obj_brick;
ascii_dictionary[3 ] = obj_quicksand;
ascii_dictionary[4 ] = obj_noteblock;
ascii_dictionary[5 ] = obj_flipblock;
ascii_dictionary[6 ] = obj_digsand;
ascii_dictionary[7 ] = obj_donut_red;
ascii_dictionary[8 ] = obj_fencetop;
ascii_dictionary[9 ] = obj_muncher;
ascii_dictionary[10] = obj_vine;
ascii_dictionary[11] = obj_net;
ascii_dictionary[12] = obj_probability_tile;

/// Finally call event for rest of generation
// A child object should be able to only change the create event
event_user(0);
