/// VERY IMPORTANT Generation properties

show_debug_message("Default create...");

// Name at the start of every text file to check for generation
file_location = "LevelGen_Files\\cave\\"
level_type = "area";
// How many different types of paths are there for every path (for example, 8 types of "T" and "+")
max_variations = 8;

/// CHANCES
// all of this numbers mean the probability of selecting 1 out of X

//chance to mirror the selected room.
chance_to_flip_subroom = 2; // 1 out of...
chance_to_eliminate_muncher = 5;

// brick to question block
chance_to_substitute_brick_with_question_block = 10; // 1 out of...
// array of possible outcomes of random_sprout()
block_array[0] = 1;
block_array[1] = 1;
block_array[2] = 1;
block_array[3] = 2;
block_array[4] = 2;

// enemies in floored tiles
chance_to_spawn_floored_enemy = 20;
floor_enemies_array[0] = obj_thwomp;
floor_enemies_array[1] = obj_koopa_red;
floor_enemies_array[2] = obj_koopa;
floor_enemies_array[3] = obj_goomba;
floor_enemies_array[4] = obj_galoomba;
floor_enemies_array[5] = obj_buzzybeetle;
floor_enemies_array[6] = obj_nokobon;
floor_enemies_array[7] = obj_jumpingkoopa;

// enemies in ceiling tiles
chance_to_spawn_ceiling_enemy = 60;
ceiling_enemies_array[0] = obj_buzzy_ceiling;
ceiling_enemies_array[1] = obj_swooper;
ceiling_enemies_array[2] = obj_swooper;
ceiling_enemies_array[3] = obj_swooper;
ceiling_enemies_array[4] = obj_swooper;

// enemies in fence tiles
chance_to_spawn_fence_enemy = 40;
fence_enemies_array[0] = obj_netkoopa;
fence_enemies_array[1] = obj_netkoopa_red;

/// DICTIONARY
// This transforms the numbers in the .txt into real game objects

ascii_dictionary[0] = noone; // 0 is normally air
ascii_dictionary[1] = obj_unspinbrick;
ascii_dictionary[2] = obj_brick;
ascii_dictionary[3] = obj_vine;
ascii_dictionary[4] = obj_noteblock;
ascii_dictionary[5] = obj_flipblock;
ascii_dictionary[6] = obj_platform_falling;
ascii_dictionary[7] = obj_donut;
ascii_dictionary[8] = obj_net;
ascii_dictionary[9] = obj_fencetop;
ascii_dictionary[10] = obj_muncher;

/// Finally call event for rest of generation
// A child object should be able to only change the create event
event_user(0);
