///scr_place_opening_below()
if( room_types[row, col] == 1 )
{
    if( (row > 0) && ((room_types[row-1, col]==2)||(room_types[row-1, col]==4)) )
        room_types[row, col] = 4;
    else
        room_types[row, col] = 2;
    
}
else if( room_types[row, col] == 3 )
    room_types[row, col] = 4;
else
    show_debug_message("ERROR: scr_place_opening_below()");
