x = xstart;
y = ystart;

mask_index = spr_empty;

/// up
with( collision_square(x, y-16, 16, obj_blockparent) )
{
    scr_4bitmasking_terrain();
}

/// down
with( collision_square(x, y+16, 16, obj_blockparent) )
{
    scr_4bitmasking_terrain();
}

/// left
with( collision_square(x-16, y, 16, obj_blockparent) )
{
    scr_4bitmasking_terrain();
}

/// right
with( collision_square(x+16, y, 16, obj_blockparent) )
{
    scr_4bitmasking_terrain();
}

// area re-check
var _x1 = x-16;
var _x2 = x+32;
var _y1 = y-16;
var _y2 = y+32;
//debug_draw_rect(_x1, _y1, _x2, _y2);
var list_temp = collision_rectangle_list(_x1,
                                            _y1,
                                            _x2,
                                            _y2,
                                            obj_unspinbrick,
                                            true,
                                            true);
if(list_temp)
{
    for(var i = 0; i < ds_list_size(list_temp); i++)
    {
        with( ds_list_find_value(list_temp, i) )
        {
            scr_4bitmasking_terrain();
        }
    }
    ds_list_destroy(list_temp);
}


event_inherited();
