///scr_overlaymanagement(sprite_index,image_index,x,y,xscale,yscale,rot,color,alpha)

var o_sprite_index = argument0;
var o_image_index = argument1;
var o_x = argument2;
var o_y = argument3;
var o_xscale = argument4;
var o_yscale = argument5;
var o_rot = argument6;
var o_color = argument7;
var o_alpha = argument8;

if(global.cosmetic_vega)
{
    var sprite_overlay = spr_empty;
    
    if(o_sprite_index == scr_mariomap())
    {
        sprite_overlay = spr_mariovega_map;
    }
    else if(o_sprite_index == spr_mario_frog_walk)
    {
        sprite_overlay = spr_mariovega_frog_walk;
    }
    else if(o_sprite_index == scr_mariowalk())
    {
        sprite_overlay = spr_mariovega_big_walk;
    }
    else if(o_sprite_index == scr_mariojump())
    {
        sprite_overlay = spr_mariovega_big_jump;
    }
    else if(o_sprite_index == scr_mariorun())
    {
        sprite_overlay = spr_mariovega_big_run;
    }
    else if(o_sprite_index == scr_mariorunjump())
    {
        sprite_overlay = spr_mariovega_big_runjump;
    }
    else if(o_sprite_index == scr_marioskid())
    {
        sprite_overlay = spr_mariovega_big_skid;
    }
    else if(o_sprite_index == scr_marioclimb())
    {
        sprite_overlay = spr_mariovega_big_climb;
    }
    else if( (o_sprite_index == spr_mario_frog_swim) || (o_sprite_index == spr_mario_penguin_swim) )
    {
        sprite_overlay = spr_mariovega_big_swim1;
    }
    else if(o_sprite_index == scr_marioswim())
    {
        sprite_overlay = spr_mariovega_big_swim;
    }
    else if(o_sprite_index == scr_marioswim3())
    {
        sprite_overlay = spr_mariovega_big_swim3;
    }
    else if(o_sprite_index == scr_mariohold())
    {
        sprite_overlay = spr_mariovega_big_hold;
    }
    else if(o_sprite_index == scr_mariocarry())
    {
        sprite_overlay = spr_mariovega_big_carry;
    }
    else if(o_sprite_index == scr_marioduck())
    {
        sprite_overlay = spr_mariovega_big_duck;
    }
    else if(o_sprite_index == scr_mariogoal())
    {
        sprite_overlay = spr_mariovega_big_goal;
    }
    else if(o_sprite_index == scr_mariowarp())
    {
        sprite_overlay = spr_mariovega_big_warp;
    }
    else if(o_sprite_index == scr_marioshoot())
    {
        sprite_overlay = spr_mariovega_big_shoot;
    }
    else if(o_sprite_index == scr_marioslide())
    {
        sprite_overlay = spr_mariovega_big_slide;
    }
    else if(o_sprite_index == scr_mariokick())
    {
        sprite_overlay = spr_mariovega_big_kick;
    }
    else if(o_sprite_index == scr_marioride())
    {
        sprite_overlay = spr_mariovega_big_ride;
    }
    else if( (o_sprite_index == spr_mario_squirrel_float) || (o_sprite_index == spr_mario_crown_fall) || (o_sprite_index == spr_mario_crown_float) )
    {
        sprite_overlay = spr_mariovega_big_float;
    }
    else if( (o_sprite_index == spr_mario_karate_melee) )
    {
        sprite_overlay = spr_mariovega_big_karate_melee;
    }
    else if( (o_sprite_index == spr_mario_pull) )
    {
        sprite_overlay = spr_mariovega_big_pull;
    }
    else if( (o_sprite_index == spr_mario_ninja_wallclimb) )
    {
        sprite_overlay = spr_mariovega_ninja_wallclimb;
    }
    else if( (o_sprite_index == spr_mario_swooper_flap) )
    {
        sprite_overlay = spr_mariovega_swooper_flap;
    }
    else if( (o_sprite_index == spr_mario_propeller_fly) )
    {
        sprite_overlay = spr_mariovega_propeller_fly;
    }
    else if( (o_sprite_index == spr_mario_rock_roll) )
    {
        sprite_overlay = spr_mariovega_rock_roll;
    }
    else if( (o_sprite_index == spr_mario_leaf_spin) || (o_sprite_index == spr_mario_tanooki_spin) )
    {
        sprite_overlay = spr_mariovega_spin;
    }
    
    if(o_sprite_index != scr_mariomap())
    {
        if global.powerup == cs_small
            o_y += 8;
        else if global.powerup == cs_crown
            o_y -= 2;
    }
    
    if(sprite_overlay != spr_empty)
    {
        draw_sprite_ext(sprite_overlay,
                        o_image_index,
                        o_x,
                        o_y,
                        o_xscale,
                        o_yscale,
                        o_rot,
                        o_color,
                        o_alpha);
    }
}

if(global.powerup_specs)
{
    var sprite_overlay = spr_empty;
    
    if(o_sprite_index == scr_mariomap())
    {
        sprite_overlay = spr_mariospec_map;
    }
    else if(o_sprite_index == spr_mario_frog_walk)
    {
        sprite_overlay = spr_mariospec_frogwalk;
    }
    else if(o_sprite_index == scr_mariowalk())
    {
        sprite_overlay = spr_mariospec_walk;
    }
    else if(o_sprite_index == scr_mariojump())
    {
        sprite_overlay = spr_mariospec_jump;
    }
    else if(o_sprite_index == scr_mariorun())
    {
        sprite_overlay = spr_mariospec_run;
    }
    else if(o_sprite_index == scr_mariorunjump())
    {
        sprite_overlay = spr_mariospec_runjump;
    }
    else if(o_sprite_index == scr_marioskid())
    {
        sprite_overlay = spr_mariospec_skid;
    }
    else if(o_sprite_index == scr_marioclimb())
    {
        sprite_overlay = spr_mariospec_climb;
    }
    else if( (o_sprite_index == spr_mario_frog_swim) || (o_sprite_index == spr_mario_penguin_swim) )
    {
        sprite_overlay = spr_mariospec_swim1;
    }
    else if(o_sprite_index == scr_marioswim())
    {
        sprite_overlay = spr_mariospec_swim;
    }
    else if(o_sprite_index == scr_marioswim3())
    {
        sprite_overlay = spr_mariospec_swim3;
    }
    else if(o_sprite_index == scr_mariohold())
    {
        sprite_overlay = spr_mariospec_hold;
    }
    else if(o_sprite_index == scr_mariocarry())
    {
        sprite_overlay = spr_mariospec_carry;
    }
    else if(o_sprite_index == scr_marioduck())
    {
        sprite_overlay = spr_mariospec_duck;
    }
    else if(o_sprite_index == scr_mariogoal())
    {
        sprite_overlay = spr_mariospec_goal;
    }
    else if(o_sprite_index == scr_mariowarp())
    {
        sprite_overlay = spr_mariospec_warp;
    }
    else if(o_sprite_index == scr_marioshoot())
    {
        sprite_overlay = spr_mariospec_shoot;
    }
    else if(o_sprite_index == scr_marioslide())
    {
        sprite_overlay = spr_mariospec_slide;
    }
    else if(o_sprite_index == scr_mariokick())
    {
        sprite_overlay = spr_mariospec_kick;
    }
    else if(o_sprite_index == scr_marioride())
    {
        sprite_overlay = spr_mariospec_ride;
    }
    else if( (o_sprite_index == spr_mario_squirrel_float) || (o_sprite_index == spr_mario_crown_fall) || (o_sprite_index == spr_mario_crown_float) )
    {
        sprite_overlay = spr_mariospec_float;
    }
    else if( (o_sprite_index == spr_mario_karate_melee) )
    {
        sprite_overlay = spr_mariospec_karate_melee;
    }
    else if( (o_sprite_index == spr_mario_pull) )
    {
        sprite_overlay = spr_mariospec_pull;
    }
    else if( (o_sprite_index == spr_mario_ninja_wallclimb) )
    {
        sprite_overlay = spr_mariospec_ninja_wallclimb;
    }
    else if( (o_sprite_index == spr_mario_swooper_flap) )
    {
        sprite_overlay = spr_mariospec_swooper_flap;
    }
    else if( (o_sprite_index == spr_mario_propeller_fly) )
    {
        sprite_overlay = spr_mariospec_fly;
    }
    else if( (o_sprite_index == spr_mario_rock_roll) )
    {
        sprite_overlay = spr_mariospec_roll;
    }
    else if( (o_sprite_index == spr_mario_leaf_spin) || (o_sprite_index == spr_mario_tanooki_spin) )
    {
        sprite_overlay = spr_mariospec_spin;
    }
    
    if(o_sprite_index != scr_mariomap())
    {
        if global.powerup == cs_small
            o_y += 9;
        else if global.powerup == cs_crown
            o_y -= 3.5;
    }
    
    if(sprite_overlay != spr_empty)
    {
        draw_sprite_ext(sprite_overlay,
                        o_image_index,
                        o_x,
                        o_y,
                        o_xscale,
                        o_yscale,
                        o_rot,
                        o_color,
                        o_alpha);
    }
}
