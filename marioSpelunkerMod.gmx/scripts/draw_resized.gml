/// draw_resized(offset, multiplier)

draw_sprite_ext(sprite_index,
                image_index,
                x,
                y,
                1,
                1,
                image_angle,
                image_blend,
                image_alpha);
/*
draw_sprite_ext(sprite_index,
                image_index,
                x - 1,
                y - 1,
                1.125,
                1.125,
                image_angle,
                image_blend,
                image_alpha);
