/// audio_play_sound_proximity(soundid,priority,loops)
var playit = false;
if( instance_exists(obj_mario) )
{
    if( point_distance(x, y, obj_mario.x, obj_mario.y) < view_hview[0] )
        playit = true;
}
else
    playit = true;

if playit
    audio_play_sound(argument0, argument1, argument2);
