var physicsparent = collision_rectangle(bbox_left,bbox_top-1,bbox_right,bbox_top,obj_physicsparent,0,0);
if( variable_instance_exists(physicsparent, "hardness") && (physicsparent.hardness < 99) )
{
    with physicsparent
    {
        vspeed = -5.2;
    }
}
