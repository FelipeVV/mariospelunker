///scr_powerupkoopa(obj to evolve to koopa)
with( instance_place(x, y, argument0) )
{
    var powerup_id = scr_sprite_get_constant(other.sprite_index);
    var inscreate = noone;
    switch(powerup_id)
    {
        case cs_fire:
        case cs_pyre:
            inscreate = obj_firebro;
            break;
        case cs_hammer:
            inscreate = obj_hammerbro;
            break;
        case cs_boomerang:
            inscreate = obj_boomerangbro;
            break;
        case cs_big:
        case cs_super:
            inscreate = obj_koopa_hyper;
            break;
        case cs_propeller:
        case cs_frog:
        case cs_swooper:
        case cs_bee:
            inscreate = obj_jumpingkoopa;
            break;
        case cs_karate:
            inscreate = obj_sumobro;
            break;
        case cs_1up:
        case cs_rock:
            inscreate = obj_sledgebro;
            break;
        case cs_walljump:
            inscreate = obj_chargingchuck;
            break;
        case cs_bomb:
            inscreate = obj_mechakoopa;
            break;
        case cs_star:
        case cs_crown:
            inscreate = obj_magikoopa;
            break;
            
    }
    if(inscreate != noone)
    {
        //Play powerup sound
        audio_play_sound(snd_powerup,0,0)
        instance_create(x, y, obj_sparkletrail)
        instance_create(x, y, inscreate);
        with(other){ instance_destroy(); }
        instance_destroy();
    }
}
