///scr_constant_get_sprite(variable)

/*
**  Usage:
**      scr_constant_get_sprite(variable)
**
**  Given:
**      variable    A constant
**
**  Returns:
**      The sprite_index that represents that constant.
*/
switch argument0
{
    case cs_big: return "mushroom"
    case cs_fire: return "fire flower"
    case cs_hammer: return "hammer suit"
    case cs_leaf: return "leaf"
    case cs_tanooki: return "tanooki suit"
    case cs_frog: return "frog suit"
    case cs_bomb: return "bombshroom"
    case cs_ice: return "ice flower"
    case cs_super: return "super flower"
    case cs_carrot: return "carrot"
    case cs_ninja: return "ninja suit"
    case cs_bee: return "beeshroom"
    case cs_shell: return "blue shell"
    case cs_penguin: return "penguin suit"
    case cs_propeller: return "propellershroom"
    case cs_boomerang: return "boomerang flower"
    case cs_walljump: return "walljumpshroom"
    case cs_squirrel: return "acorn"
    case cs_cloud: return "cloud flower"
    case cs_rock: return "rockshroom"
    case cs_pyre: return "pyre flower"
    case cs_swooper: return "swooper suit"
    case cs_karate: return "karate suit"
    case cs_crown: return "estrogen"
    case cs_greenyoshi: return "yoshi"
    case cs_redyoshi: return "red yoshi"
    case cs_yellowyoshi: return "yellow yoshi"
    case cs_blueyoshi: return "blue yoshi"
    case cs_kuriboshoe: return "kuriboshoe"
    case cs_baburushoe: return "baburushoe"
    case cs_dossunshoe: return "dossunshoe"
    case cs_jugemushoe: return "jugemushoe"
    case cs_1up: return "1up"
    case cs_3up: return "3upmoon"
    case cs_poison: return "poisonshroom"
    case cs_star: return "star"
    case cs_key: return "key"
    case cs_spring: return "spring"
    case cs_superspring: return "super spring"
    case cs_pswitch: return "pswitch"
    case cs_eswitch: return "eswitch"
    case cs_propellerblock: return "propeller block"
    case cs_beanstalk: return "bean stalk"
    case cs_specs: return "spectacles"
    case cs_helmet: return "spiky helmet"
    case cs_springs: return "jumping springs"
    case cs_coffee: return "speedy coffee"
    case cs_spikedshoes: return "non-slip shoes"
    case cs_weights: return "stomping ankle weights"
    case cs_compass: return "compass"
    case cs_vega: return "vega's mask"
    default: return "item"
}
