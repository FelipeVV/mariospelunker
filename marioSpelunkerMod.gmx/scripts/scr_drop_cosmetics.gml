///scr_drop_cosmetics();

if(global.cosmetic_vega)
{
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_vegamask;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_specs)
{
    global.powerup_specs = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_specs;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_spikyhelmet)
{
    global.powerup_spikyhelmet = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_spikyhelmet;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_springs)
{
    global.powerup_springs = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_springs;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_coffee)
{
    global.powerup_coffee = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_coffee;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_spikedshoes)
{
    global.powerup_spikedshoes = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_spikedshoes;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_rockleeweights)
{
    global.powerup_rockleeweights = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_weights;
        direction = random_range(90-droprange, 90+droprange);
    }
}

if(global.powerup_compass)
{
    global.powerup_compass = false;
    with( instance_create(x, y, obj_shard) )
    {
        var droprange = 25;
        sprite_index = spr_compass;
        direction = random_range(90-droprange, 90+droprange);
    }
}
