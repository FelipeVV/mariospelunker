///flip_section(string to flip)

var str = argument0;
var str2 = "";
var list = string_parse(str, "#", true);

for(var i = 0; i < ds_list_size(list); i++)
{
    //show_debug_message(ds_list_find_value(list, i));
    var substr = ds_list_find_value(list, i);
    str2 += string_reverse(substr);
    str2 += "#";
}

ds_list_destroy(list);

return str2;
