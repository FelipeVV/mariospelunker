///scr_placedefault_1or3()
// pone una pieza tipo 1 o 3, dependiendo del tile arriba

// En el caso en el que arriba tenemos una "T"(2), ahora ocupamos poner una "T" inversa (3)
if( (row > 0) && ((room_types[row-1, col]==2)||(room_types[row-1, col]==4)) )
{
    room_types[row, col] = 3;
}
else
{
    if(room_types[row, col] != 3)
        room_types[row, col] = 1;
}
