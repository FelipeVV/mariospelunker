/// array_shuffle(array)
var i, j, k;
var data = argument0;
var size = array_length_1d(data);

for (i = 0; i < size; i += 1)
{
    j = irandom_range(i, size - 1)
    if (i != j)
    {
        k = data[i]
        data[i] = data[j]
        data[j] = k
    }
}
