var sproutins = cs_poison;
switch(argument0)
{
    default:
    case 0: // shroom if small, coin if big (code in object itself)
        sproutins = cs_coin;
        break;
    case 1: // shroom is implicit
        sproutins = choose(cs_fire, 
                            cs_fire,
                            cs_hammer,
                            cs_ice,
                            cs_carrot,
                            cs_super, 
                            cs_boomerang,
                            cs_penguin,
                            cs_spring,
                            cs_kuriboshoe,
                            cs_baburushoe);
        break;
    case 2:
        sproutins = choose(cs_walljump,
                            cs_walljump,
                            cs_bomb,
                            cs_bomb,
                            cs_shell,
                            cs_shell,
                            cs_ninja,
                            cs_squirrel,
                            cs_propellerblock,
                            choose(cs_kuriboshoe, cs_baburushoe, cs_dossunshoe, cs_jugemushoe));
        break;
    case 3:
        sproutins = choose(cs_leaf,
                            cs_tanooki,
                            cs_karate,
                            cs_propeller,
                            cs_bee,
                            cs_propeller,
                            cs_cloud,
                            cs_crown,
                            choose(cs_kuriboshoe, cs_baburushoe, cs_dossunshoe, cs_jugemushoe)
                            );
        break;
    case 4:
        sproutins = choose(cs_rock,
                            cs_pyre,
                            cs_karate,
                            cs_star);
        break;
    case 5:
        sproutins = choose(cs_greenyoshi,
                            cs_redyoshi,
                            cs_yellowyoshi,
                            cs_blueyoshi);
        break;
    case 6:
        sproutins = choose(cs_frog,
                            cs_frog,
                            cs_penguin,
                            cs_penguin,
                            cs_leaf,
                            cs_ice);
        break;
    case 7:
        sproutins = choose(cs_walljump,
                            cs_bomb,
                            cs_bomb,
                            cs_shell,
                            cs_squirrel,
                            cs_propellerblock);
        break;
    /// shop basic
    case 8:
        sproutins = choose(cs_walljump,
                            cs_squirrel,
                            cs_bomb,
                            cs_shell,
                            cs_ninja,
                            cs_tanooki,
                            cs_leaf,
                            cs_crown
                            );
        break;
    /// shop based
    case 9:
        sproutins = choose(cs_rock,
                            cs_star,
                            cs_bee,
                            cs_cloud,
                            cs_rock,
                            cs_star,
                            cs_bee,
                            cs_cloud,
                            cs_1up
                            );
        if(sproutins==cs_1up && (random(30)<1))
            sproutins = cs_3up
        break;
    /// SHOP yoshi
    case 10:
        sproutins = choose(cs_greenyoshi,
                            cs_redyoshi,
                            cs_yellowyoshi,
                            cs_blueyoshi);
        break;
    /// shop shoes
    case 11:
        sproutins = choose(cs_kuriboshoe,
                            cs_baburushoe,
                            cs_dossunshoe,
                            cs_jugemushoe,
                            cs_baburushoe,
                            cs_dossunshoe,
                            cs_jugemushoe,
                            cs_baburushoe,
                            cs_dossunshoe,
                            cs_jugemushoe);
        break;
    /// shop spelunky permanent
    case 12:
        sproutins = choose(cs_specs,
                            cs_helmet,
                            cs_springs,
                            cs_coffee,
                            cs_spikedshoes,
                            cs_weights,
                            cs_compass);
        break;
    case 13: // ice caves sprouts
        sproutins = choose(cs_ice,
                            cs_carrot, 
                            cs_penguin,
                            cs_spring,
                            cs_cloud,
                            cs_propellerblock,
                            cs_ninja,
                            cs_bomb);
        
}

if ((argument0<8)||(argument0>12))// if not a shop item
{
    if(random(30)<1)
        sproutins = random_sprout(12);
    else if ( random(30)<1 )
         sproutins = random_sprout(10);
    else if ( random(90)<1 )
        sproutins = random_sprout(4);
    else if ( random(150)<1 )
    {
        if( random(50)<1 )
            sproutins = cs_3up;
        else
            sproutins = cs_1up;
    }
}


return sproutins;

/*
TIER building

reference: scr_constant_get_sprite()

untieres (useless)
spr_poison
spr_pswitch
spr_eswitch
spr_frogsuit
spr_beanstalk
spr_key
spr_1up
spr_3upmoon

tier 0: shroom OR COIN
spr_mushroom

tier 1: better than mushroom
spr_fireflower
spr_hammersuit
spr_iceflower
spr_carrot
spr_superflower
spr_boomerangflower

tier 2: medium mobility
spr_leaf
spr_tanookisuit
spr_bombshroom
spr_blueshell
spr_penguinsuit
spr_acorn
spr_spring
spr_propellerblock

tier 3: high movility
spr_ninjasuit
spr_beeshroom
spr_propellershroom
spr_walljumpshroom
spr_cloudflower
spr_swoopersuit
spr_supercrown
spr_kuriboshoe
spr_baburushoe
spr_dossunshoe
spr_jugemushoe
spr_spring_super

tier 4: high damage output
spr_rockshroom
spr_pyreflower
spr_karatesuit
spr_star

tier 5: yoshi
spr_hatch
spr_hatch_r
spr_hatch_y
spr_hatch_b











