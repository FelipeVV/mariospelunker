///scr_constant_get_price(variable)

/*
**  Usage:
**      scr_constant_get_price(variable)
**
**  Given:
**      variable    A constant
**
**  Returns:
**      The price that represents that constant.
*/
switch argument0
{
    case cs_big: return 15
    case cs_fire: return 25
    case cs_hammer: return 25
    case cs_leaf: return 35
    case cs_tanooki: return 35
    case cs_frog: return 30
    case cs_bomb: return 38
    case cs_ice: return 30
    case cs_super: return 30
    case cs_carrot: return 30
    case cs_ninja: return 35
    case cs_bee: return 35
    case cs_shell: return 35
    case cs_penguin: return 35
    case cs_propeller: return 40
    case cs_boomerang: return 28
    case cs_walljump: return 35
    case cs_squirrel: return 35
    case cs_cloud: return 35
    case cs_rock: return 45
    case cs_pyre: return 35
    case cs_swooper: return 25
    case cs_karate: return 35
    case cs_crown: return 35
    case cs_greenyoshi: return 38
    case cs_redyoshi: return 40
    case cs_yellowyoshi: return 40
    case cs_blueyoshi: return 40
    case cs_kuriboshoe: return 25
    case cs_baburushoe: return 30
    case cs_dossunshoe: return 30
    case cs_jugemushoe: return 30
    case cs_1up: return 120
    case cs_3up: return 340
    case cs_poison: return 2
    case cs_star: return 100
    case cs_key: return 100
    case cs_spring: return 10
    case cs_superspring: return 20
    case cs_pswitch: return 100
    case cs_eswitch: return 100
    case cs_propellerblock: return 25
    case cs_beanstalk: return 1000000
    /// Permanent powerups
    case cs_specs: return 50
    case cs_helmet: return 55
    case cs_springs: return 60
    case cs_coffee: return 60
    case cs_spikedshoes: return 58
    case cs_weights: return 65
    case cs_compass: return 50
    case cs_vega: return 10
    default: return 5
}
