// choose a different column than the spawning Mario
var col_secret_door = col_spawn;
while(col_secret_door == col_spawn){ col_secret_door = irandom(3); }
var row_secret_door = irandom(2); // last level not included
var secret_door_room_col = 10;
var secret_door_room_row = 10;
with(instance_create(x + (col_secret_door*352) + (secret_door_room_col*16),
                y + (row_secret_door*288) + (secret_door_room_row*16),
                obj_secret_bomb_door))
{
    destroy_objects_in_area(x, y, x+16, y+16, obj_blockparent, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_fencetop, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_unspinbrick, false);
    destroy_objects_in_area(x, y, x+16, y+16, obj_net, false);
    //destroy_objects_in_area(x-16, y, x+16, y+32, obj_enemyparent, false);
    
    // extra platform for standing
    with(instance_create(x, y + 16, obj_fencetop)){ sprite_index = spr_platformtemp2; }
    with(instance_create(x - 16, y + 16, obj_fencetop)){ sprite_index = spr_platformtemp2; }
    with(instance_create(x + 16, y + 16, obj_fencetop)){ sprite_index = spr_platformtemp2; }
}
