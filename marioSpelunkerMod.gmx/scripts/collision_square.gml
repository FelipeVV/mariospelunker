/// collision_square(x, y, size, object) assumes precise and notme

var _x = argument0;
var _y = argument1;

var size = argument2;
var obj = argument3;

return collision_rectangle(_x, _y, _x+size, _y+size, obj, true, true);
