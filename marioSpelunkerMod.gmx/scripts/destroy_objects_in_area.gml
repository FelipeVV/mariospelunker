/// destroy_objects_in_area(x1, y1, x2, y2, object to destroy, debug draw rectangle?)

if(argument5) debug_draw_rect(argument0, argument1, argument2, argument3);
var list_temp = collision_rectangle_list(argument0,
                                            argument1,
                                            argument2,
                                            argument3,
                                            argument4,
                                            true,
                                            true);
if(list_temp)
{
    destroy_objects_in_list( list_temp );
    ds_list_destroy(list_temp);
}
