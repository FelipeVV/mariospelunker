///spawn_random_enemy(x, y, object)
// Does instance_create but fancier:
//  applying custom protocols to each kind of enemy
var _x = argument0;
var _y = argument1;
var insenemy = argument2;

/// basic protocol for specific types of enemy
if ( insenemy == obj_urchin )
{
    var i = irandom_range(6, 10)*16;
    if( random(4) < 1 )
    {
        // horizontal
        instance_create(_x, _y, obj_right);
        instance_create(_x-16, _y, obj_endmarker);
        instance_create(_x, _y, insenemy);
        instance_create(_x+i, _y, obj_left);
        instance_create(_x+16+i, _y, obj_endmarker);
        destroy_objects_in_area(_x, _y, _x+i+16, _y+32, obj_blockparent, false);
    }
    else
    {
        // vertical
        instance_create(_x, _y, obj_down);
        instance_create(_x, _y-16, obj_endmarker);
        instance_create(_x, _y, insenemy);
        instance_create(_x, _y+i, obj_up);
        instance_create(_x, _y+16+i, obj_endmarker);
        destroy_objects_in_area(_x, _y, _x+32, _y+i+16, obj_blockparent, false);
    }

}
else if ( insenemy == obj_pokey )
{
    with(instance_create(_x, _y, insenemy))
    {
        height = choose(2, 3, 3, 4);
        for(var i = 0; i < height; i++)
        {
            instance_create(x, y - (16*(height+i)), obj_pokey);
        }
        destroy_objects_in_area(x, y-(height*16), x+16, y, obj_blockparent, false);
    }
}
// koopa
else if( (insenemy == obj_koopa_red) || (insenemy == obj_koopa) || (insenemy == obj_jumpingkoopa) )
{
    with(instance_create(_x+8, _y, insenemy))
    {
        if(random(2) < 1) hspeed *= -1;
    }
}
// thwomp
else if(insenemy == obj_thwomp)
{
    var i = (choose(4, 4, 5, 5, 6)+1) * 16;
    if( !collision_rectangle(_x, _y-(i+16), _x+32, _y, obj_unspinbrick_artificial, 1, 1)
        && ( _y-(i+16) > 0 )
        )
    {
        instance_create(_x, _y - i, insenemy);
        destroy_objects_in_area(_x, _y-(i+16), _x+32, _y, obj_blockparent, false);
        destroy_objects_in_area(_x, _y-(i+16), _x+32, _y, obj_fencetop, false);
    }
    else
        show_debug_message("thwomp spawning failed");
}
else if( (insenemy == obj_goomba) || (insenemy == obj_galoomba) || (insenemy == obj_paragoomba) || (insenemy == obj_cheepcheep) || (insenemy == obj_spikedcheep) || (insenemy == obj_fishbone) )
{
    // stupid dude turn your ass for once
    with(instance_create(_x, _y, insenemy))
    {
        if(random(2) < 1) hspeed *= -1;
    }
}
else if( insenemy == obj_fallingspike ) && collision_square(_x, _y - 16, 16, obj_fallingspike)
{
    if collision_square(_x-16, _y - 32, 16, obj_solidtop)
        instance_create(_x-16, _y-16, insenemy);
    else if collision_square(_x+16, _y - 32, 16, obj_solidtop)
        instance_create(_x+16, _y-16, insenemy);
}
// everything else
else if (insenemy != noone)
{
    instance_create(_x, _y, insenemy);
}
                    

