///scr_powerupgoomba(obj to evolve to goomba)
with( instance_place(x, y, argument0) )
{
    var powerup_id = scr_sprite_get_constant(other.sprite_index);
    var inscreate = noone;
    switch(powerup_id)
    {
        case cs_walljump:
        case cs_big:
        case cs_bee:
        case cs_swooper:
            inscreate = obj_paragoomba;
            break;
        case cs_propeller:
            inscreate = obj_bubblegoomba;
            break;
        case cs_kuriboshoe:
        case cs_baburushoe:
        case cs_dossunshoe:
        case cs_jugemushoe:
            inscreate = obj_shoegoomba;
            break;
            
    }
    
    if(inscreate != noone)
    {
        //Play powerup sound
        audio_play_sound(snd_powerup,0,0)
        instance_create(x, y, obj_sparkletrail)
        instance_create(x, y, inscreate);
        with(other){ instance_destroy(); }
        instance_destroy();
    }
}
