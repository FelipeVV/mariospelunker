var _x = argument0;
var _y = argument1;
var insenemy = choose(obj_netkoopa, obj_netkoopa,
                    obj_netkoopa_red, obj_netkoopa_red,
                    obj_netkoopa_hyper);

if( (insenemy == obj_koopa_red) || (insenemy == obj_koopa) || (insenemy == obj_jumpingkoopa) )
{
    with(instance_create(_x, _y, insenemy))
    {
        y -= 16;
    }
}
else if(insenemy == obj_thwomp)
{
    var i = (choose(2, 2, 3, 3, 5, 5)+1) * 16;
    instance_create(_x, _y - i, insenemy);
    var list_temp = collision_rectangle_list(_x, _y-i, _x+32, _y, obj_blockparent, true, true);
    if(list_temp)
    {
        destroy_objects_in_list( list_temp );
        ds_list_destroy(list_temp);
    }
}
else
{
    instance_create(_x, _y, insenemy);
}
                    

