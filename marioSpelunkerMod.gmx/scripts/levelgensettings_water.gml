/// VERY IMPORTANT Generation properties

show_debug_message("Water create event ");

// Name at the start of every text file to check for generation
file_location = "LevelGen_Files\\water\\"
level_type = "water";
// How many different types of paths are there for every path (for example, 8 types of "T" and "+")
max_variations = 6;

// Should me create the extra secret door?
secret_exit_door = false;
secret_boo_exit_door = false;
// Is this a shortcut level that will need to unlock a door upon ending?
secret_area = true;

/// CHANCES
// all of this numbers mean the probability of selecting 1 out of X

//chance to mirror the selected room.
chance_to_flip_subroom = 2; // 1 out of...
chance_to_eliminate_muncher = 5;
chance_to_eliminate_jelectro = 1.8;
chance_to_place_probability_tile = 7;

// Flavours! Special stuff that can happen in levels
chance_for_flavor_angrysun = 0;
chance_for_flavor_lakitu = 0;
chance_for_flavor_bobombrain = 0;
chance_for_flavor_storm = 0;
can_spawn_flying_bobombs = false; // always false
chance_to_spawn_flavor_bobombs = 140;


chance_to_substitute_unspin_brick_with_question_block = 20;
// array of possible outcomes of random_sprout()
block_array[0] = 1;
block_array[1] = 1;
block_array[2] = 1;
block_array[3] = 6; // 6 is custom for water invironments
block_array[4] = 6;
block_array[5] = 6;

// enemies in floored tiles
chance_to_spawn_floored_enemy = 60;
floor_enemies_array[0 ] = obj_goomba;
floor_enemies_array[1 ] = obj_vulcanolotus;
floor_enemies_array[2 ] = obj_thwomp;
floor_enemies_array[3 ] = obj_thwomp;
floor_enemies_array[4 ] = obj_koopa;
floor_enemies_array[5 ] = obj_koopa_red;
floor_enemies_array[6 ] = obj_jumpingkoopa;
floor_enemies_array[7 ] = obj_jumpingkoopa;
floor_enemies_array[8 ] = obj_buzzybeetle;
floor_enemies_array[9 ] = obj_spiny;
floor_enemies_array[10] = obj_spike;

// enemies in ceiling tiles
chance_to_spawn_ceiling_enemy = 80;
ceiling_enemies_array[0] = obj_swooper;
ceiling_enemies_array[1] = obj_swooper;
ceiling_enemies_array[2] = obj_fallingspike;
ceiling_enemies_array[3] = obj_fallingspike;

// enemies in fence tiles
chance_to_spawn_fence_enemy = 1; // no importa
fence_enemies_array[0] = noone;

// enemies in water tiles
chance_to_spawn_water_enemy = 150;
water_enemies_array[0 ] = obj_cheepcheep;
water_enemies_array[1 ] = obj_cheepcheep;
water_enemies_array[2 ] = obj_cheepcheep;
water_enemies_array[3 ] = obj_cheepcheep;
water_enemies_array[4 ] = obj_spikedcheep;
water_enemies_array[5 ] = obj_spikedcheep;
water_enemies_array[6 ] = obj_spikedcheep;
water_enemies_array[7 ] = obj_fishbone;
water_enemies_array[8 ] = obj_fishbone;
water_enemies_array[9 ] = obj_ripvanfish;
water_enemies_array[10] = obj_blooper;
water_enemies_array[11] = obj_cheepcheep;
water_enemies_array[12] = obj_cheepcheep;

// difficult fat enemies in water tiles
// this is hold separate because the enemies are so hard and fucking annoying
chance_to_spawn_fat_water_enemy = 240;
fat_water_enemies_array[0] = obj_urchin;
fat_water_enemies_array[1] = obj_porcupuffer;
fat_water_enemies_array[2] = obj_urchin;
fat_water_enemies_array[3] = obj_porcupuffer;
fat_water_enemies_array[4] = obj_urchin;
fat_water_enemies_array[5] = obj_porcupuffer;
fat_water_enemies_array[6] = obj_torpedolaunch;

// enemies in water floor
chance_to_spawn_floored_water_enemy = 80;
floored_water_enemies_array[0] = obj_lavalotus;

/// DICTIONARY
// This transforms the numbers in the .txt into real game objects

ascii_dictionary[0] = noone; // 0 is normally air
ascii_dictionary[1] = obj_unspinbrick_water;
ascii_dictionary[2] = obj_brick;
ascii_dictionary[3] = obj_probability_tile;
ascii_dictionary[4] = obj_noteblock;
ascii_dictionary[5] = obj_fencetop;
ascii_dictionary[6] = obj_detect_bubblepipe;
ascii_dictionary[7] = obj_probability_tile;

/// Finally call event for rest of generation
// A child object should be able to only change the create event
event_user(0);
