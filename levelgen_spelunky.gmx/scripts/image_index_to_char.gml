///image_index_to_char( number to translate to char )

var num = argument0;
var ch = "Z";


if( num <= 9 )
    return string(num);

switch(num)
{
    case 10:
        ch = "A";
        break;
    case 11:
        ch = "B";
        break;
    case 12:
        ch = "C";
        break;
    case 13:
        ch = "D";
        break;
    case 14:
        ch = "E";
        break;
    case 15:
        ch = "F";
        break;
    case 16:
        ch = "G";
        break;

}

if(ch == "Z"){ show_debug_message("HUGE ERROR!!!: image_index_to_char"); }
return ch;
