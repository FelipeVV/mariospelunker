///char_to_image_index( char to translate to number )

var ch = argument0;
var num = -1;

switch(ch)
{
    case "0":
    case "1":
    case "2":
    case "3":
    case "4":
    case "5":
    case "6":
    case "7":
    case "8":
    case "9":
        num = real(ch);
        break;
    case "A":
        num = 10;
        break;
    case "B":
        num = 11;
        break;
    case "C":
        num = 12;
        break;
    case "D":
        num = 13;
        break;
    case "E":
        num = 14;
        break;
    case "F":
        num = 15;
        break;
    case "G":
        num = 16;
        break;
}

if(num == -1){ show_debug_message("HUGE ERROR!!!: char_to_image_index"); }
return num;
